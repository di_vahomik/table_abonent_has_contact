﻿CREATE TABLE [dbo].[Vahomskaya_provider] (
    [Id]    INT           IDENTITY (1, 1) NOT NULL,
    [name]  NVARCHAR (50) NOT NULL,
    [score] FLOAT (53)    NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Vahomskaya_contact] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [phone]       NVARCHAR (11) NOT NULL,
    [type]        NVARCHAR (20) NOT NULL,
    [provider_id] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Vahomskaya_contact_provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[Vahomskaya_provider] ([Id])
);

CREATE TABLE [dbo].[Vahomskaya_abonent] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [name]       NVARCHAR (50) NOT NULL,
    [surname]    NVARCHAR (50) NOT NULL,
    [patronymic] NVARCHAR (50) NOT NULL,
    [birthdate]  DATETIME      NOT NULL,
    [adress]     TEXT          NOT NULL,
    [comment]    TEXT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Vahomskaya_abonent.has_contact] (
    [abonent_id] INT NOT NULL,
    [contact_id] INT NOT NULL,
    CONSTRAINT [FK_Vahomskaya_abonent.has_contact_abonent] FOREIGN KEY ([abonent_id]) REFERENCES [dbo].[Vahomskaya_abonent] ([Id]),
    CONSTRAINT [FK_Vahomskaya_abonent.has_contact_contact] FOREIGN KEY ([contact_id]) REFERENCES [dbo].[Vahomskaya_contact] ([Id])
);

