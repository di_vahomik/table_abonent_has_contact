﻿namespace sharp
{
    partial class AbForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AbForm_SURNAME_TextBox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AbForm_NAME_TextBox = new System.Windows.Forms.TextBox();
            this.AbForm_PATRONYMIC_TextBox = new System.Windows.Forms.TextBox();
            this.AbForm_BIRTHDATE_TextBox = new System.Windows.Forms.TextBox();
            this.AbForm_ADRESS_TextBox = new System.Windows.Forms.TextBox();
            this.AbForm_COMMENT_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AbFom_CANCEL_Button = new System.Windows.Forms.Button();
            this.AbFom_OK_Button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AbForm_SURNAME_TextBox
            // 
            this.AbForm_SURNAME_TextBox.Location = new System.Drawing.Point(151, 27);
            this.AbForm_SURNAME_TextBox.Name = "AbForm_SURNAME_TextBox";
            this.AbForm_SURNAME_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_SURNAME_TextBox.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // AbForm_NAME_TextBox
            // 
            this.AbForm_NAME_TextBox.Location = new System.Drawing.Point(151, 53);
            this.AbForm_NAME_TextBox.Name = "AbForm_NAME_TextBox";
            this.AbForm_NAME_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_NAME_TextBox.TabIndex = 2;
            // 
            // AbForm_PATRONYMIC_TextBox
            // 
            this.AbForm_PATRONYMIC_TextBox.Location = new System.Drawing.Point(151, 79);
            this.AbForm_PATRONYMIC_TextBox.Name = "AbForm_PATRONYMIC_TextBox";
            this.AbForm_PATRONYMIC_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_PATRONYMIC_TextBox.TabIndex = 3;
            // 
            // AbForm_BIRTHDATE_TextBox
            // 
            this.AbForm_BIRTHDATE_TextBox.Location = new System.Drawing.Point(151, 105);
            this.AbForm_BIRTHDATE_TextBox.Name = "AbForm_BIRTHDATE_TextBox";
            this.AbForm_BIRTHDATE_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_BIRTHDATE_TextBox.TabIndex = 4;
            // 
            // AbForm_ADRESS_TextBox
            // 
            this.AbForm_ADRESS_TextBox.Location = new System.Drawing.Point(151, 131);
            this.AbForm_ADRESS_TextBox.Name = "AbForm_ADRESS_TextBox";
            this.AbForm_ADRESS_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_ADRESS_TextBox.TabIndex = 5;
            // 
            // AbForm_COMMENT_TextBox
            // 
            this.AbForm_COMMENT_TextBox.Location = new System.Drawing.Point(151, 157);
            this.AbForm_COMMENT_TextBox.Name = "AbForm_COMMENT_TextBox";
            this.AbForm_COMMENT_TextBox.Size = new System.Drawing.Size(152, 20);
            this.AbForm_COMMENT_TextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "День рождения";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Адрес";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Комментарий";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.AbFom_CANCEL_Button);
            this.groupBox1.Controls.Add(this.AbFom_OK_Button);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.AbForm_COMMENT_TextBox);
            this.groupBox1.Controls.Add(this.AbForm_ADRESS_TextBox);
            this.groupBox1.Controls.Add(this.AbForm_BIRTHDATE_TextBox);
            this.groupBox1.Controls.Add(this.AbForm_PATRONYMIC_TextBox);
            this.groupBox1.Controls.Add(this.AbForm_NAME_TextBox);
            this.groupBox1.Controls.Add(this.AbForm_SURNAME_TextBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 238);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Абонент";
            // 
            // AbFom_CANCEL_Button
            // 
            this.AbFom_CANCEL_Button.Location = new System.Drawing.Point(229, 192);
            this.AbFom_CANCEL_Button.Name = "AbFom_CANCEL_Button";
            this.AbFom_CANCEL_Button.Size = new System.Drawing.Size(74, 24);
            this.AbFom_CANCEL_Button.TabIndex = 15;
            this.AbFom_CANCEL_Button.Text = "Cancel";
            this.AbFom_CANCEL_Button.UseVisualStyleBackColor = true;
            this.AbFom_CANCEL_Button.Click += new System.EventHandler(this.AbFom_CANCEL_Button_Click);
            // 
            // AbFom_OK_Button
            // 
            this.AbFom_OK_Button.Location = new System.Drawing.Point(151, 192);
            this.AbFom_OK_Button.Name = "AbFom_OK_Button";
            this.AbFom_OK_Button.Size = new System.Drawing.Size(74, 24);
            this.AbFom_OK_Button.TabIndex = 14;
            this.AbFom_OK_Button.Text = "OK";
            this.AbFom_OK_Button.UseVisualStyleBackColor = true;
            this.AbFom_OK_Button.Click += new System.EventHandler(this.AbFom_OK_Button_Click);
            // 
            // AbForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 267);
            this.Controls.Add(this.groupBox1);
            this.Name = "AbForm";
            this.Text = "AbForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AbFom_CANCEL_Button;
        private System.Windows.Forms.Button AbFom_OK_Button;
        public System.Windows.Forms.TextBox AbForm_SURNAME_TextBox;
        public System.Windows.Forms.TextBox AbForm_NAME_TextBox;
        public System.Windows.Forms.TextBox AbForm_PATRONYMIC_TextBox;
        public System.Windows.Forms.TextBox AbForm_BIRTHDATE_TextBox;
        public System.Windows.Forms.TextBox AbForm_ADRESS_TextBox;
        public System.Windows.Forms.TextBox AbForm_COMMENT_TextBox;
    }
}