﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sharp
{
    public partial class ConForm : Form
    {
        public ConForm()
        {
            InitializeComponent();
        }

        private void AbFom_OK_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void AbFom_CANCEL_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        public Dictionary<int, string> ProviderData
        {
            set
            {
                ConForm_ComBox_Prov.DataSource = value.ToArray();
                ConForm_ComBox_Prov.DisplayMember = "Value";
            }
        }

        public int ProviderID
        {
            get { return ((KeyValuePair<int, string>)ConForm_ComBox_Prov.SelectedItem).Key; }
            set 
            {
                int idx = 0;
                foreach(KeyValuePair<int, string> item in ConForm_ComBox_Prov.Items)
                {
                    if(item.Key == value)  break;
                    idx++;
                }
                ConForm_ComBox_Prov.SelectedIndex = idx;
            }
        }
    }
}
