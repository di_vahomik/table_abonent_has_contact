﻿namespace sharp
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.kontakt = new System.Windows.Forms.TabControl();
            this.abonent = new System.Windows.Forms.TabPage();
            this.DELETE_button = new System.Windows.Forms.Button();
            this.tabl_abonent = new System.Windows.Forms.DataGridView();
            this.CHANGE_button = new System.Windows.Forms.Button();
            this.CREATE_button = new System.Windows.Forms.Button();
            this.kontak = new System.Windows.Forms.TabPage();
            this.DELETE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.CHANGE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.CREATE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.tabl_kotnakt = new System.Windows.Forms.DataGridView();
            this.provayd = new System.Windows.Forms.TabPage();
            this.DELETE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.CHANGE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.CREATE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.tabl_provayder = new System.Windows.Forms.DataGridView();
            this.spravoch = new System.Windows.Forms.TabPage();
            this.tabl_spravochnik = new System.Windows.Forms.DataGridView();
            this.FIND_PHONE_BOX = new System.Windows.Forms.TextBox();
            this.FIND_FIO_BOX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CREATE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.CHANGE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.DELETE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.kontakt.SuspendLayout();
            this.abonent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).BeginInit();
            this.kontak.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kotnakt)).BeginInit();
            this.provayd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).BeginInit();
            this.spravoch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // kontakt
            // 
            this.kontakt.Controls.Add(this.abonent);
            this.kontakt.Controls.Add(this.kontak);
            this.kontakt.Controls.Add(this.provayd);
            this.kontakt.Controls.Add(this.spravoch);
            this.kontakt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.kontakt.Location = new System.Drawing.Point(12, 26);
            this.kontakt.Name = "kontakt";
            this.kontakt.SelectedIndex = 0;
            this.kontakt.Size = new System.Drawing.Size(1037, 466);
            this.kontakt.TabIndex = 1;
            // 
            // abonent
            // 
            this.abonent.Controls.Add(this.DELETE_button);
            this.abonent.Controls.Add(this.tabl_abonent);
            this.abonent.Controls.Add(this.CHANGE_button);
            this.abonent.Controls.Add(this.CREATE_button);
            this.abonent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.abonent.Location = new System.Drawing.Point(4, 25);
            this.abonent.Name = "abonent";
            this.abonent.Padding = new System.Windows.Forms.Padding(3);
            this.abonent.Size = new System.Drawing.Size(1029, 437);
            this.abonent.TabIndex = 0;
            this.abonent.Text = "Абонент";
            this.abonent.UseVisualStyleBackColor = true;
            // 
            // DELETE_button
            // 
            this.DELETE_button.Location = new System.Drawing.Point(425, 397);
            this.DELETE_button.Name = "DELETE_button";
            this.DELETE_button.Size = new System.Drawing.Size(175, 33);
            this.DELETE_button.TabIndex = 8;
            this.DELETE_button.Text = "Удалить";
            this.DELETE_button.UseVisualStyleBackColor = true;
            this.DELETE_button.Click += new System.EventHandler(this.DELETE_button_Click);
            // 
            // tabl_abonent
            // 
            this.tabl_abonent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_abonent.Location = new System.Drawing.Point(0, 0);
            this.tabl_abonent.Name = "tabl_abonent";
            this.tabl_abonent.Size = new System.Drawing.Size(1034, 381);
            this.tabl_abonent.TabIndex = 0;
            // 
            // CHANGE_button
            // 
            this.CHANGE_button.Location = new System.Drawing.Point(220, 397);
            this.CHANGE_button.Name = "CHANGE_button";
            this.CHANGE_button.Size = new System.Drawing.Size(176, 34);
            this.CHANGE_button.TabIndex = 7;
            this.CHANGE_button.Text = "Изменить";
            this.CHANGE_button.UseVisualStyleBackColor = true;
            this.CHANGE_button.Click += new System.EventHandler(this.CHANGE_button_Click);
            // 
            // CREATE_button
            // 
            this.CREATE_button.Location = new System.Drawing.Point(17, 397);
            this.CREATE_button.Name = "CREATE_button";
            this.CREATE_button.Size = new System.Drawing.Size(176, 34);
            this.CREATE_button.TabIndex = 6;
            this.CREATE_button.Text = "Добавить абонент";
            this.CREATE_button.UseVisualStyleBackColor = true;
            this.CREATE_button.Click += new System.EventHandler(this.CREATE_button_Click);
            // 
            // kontak
            // 
            this.kontak.Controls.Add(this.DELETE_CONTACT_BUTTON);
            this.kontak.Controls.Add(this.CHANGE_CONTACT_BUTTON);
            this.kontak.Controls.Add(this.CREATE_CONTACT_BUTTON);
            this.kontak.Controls.Add(this.tabl_kotnakt);
            this.kontak.Location = new System.Drawing.Point(4, 25);
            this.kontak.Name = "kontak";
            this.kontak.Padding = new System.Windows.Forms.Padding(3);
            this.kontak.Size = new System.Drawing.Size(1029, 437);
            this.kontak.TabIndex = 1;
            this.kontak.Text = "Контакт";
            this.kontak.UseVisualStyleBackColor = true;
            // 
            // DELETE_CONTACT_BUTTON
            // 
            this.DELETE_CONTACT_BUTTON.Location = new System.Drawing.Point(414, 397);
            this.DELETE_CONTACT_BUTTON.Name = "DELETE_CONTACT_BUTTON";
            this.DELETE_CONTACT_BUTTON.Size = new System.Drawing.Size(175, 33);
            this.DELETE_CONTACT_BUTTON.TabIndex = 11;
            this.DELETE_CONTACT_BUTTON.Text = "Удалить";
            this.DELETE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.DELETE_CONTACT_BUTTON.Click += new System.EventHandler(this.DELETE_CONTACT_BUTTON_Click);
            // 
            // CHANGE_CONTACT_BUTTON
            // 
            this.CHANGE_CONTACT_BUTTON.Location = new System.Drawing.Point(209, 397);
            this.CHANGE_CONTACT_BUTTON.Name = "CHANGE_CONTACT_BUTTON";
            this.CHANGE_CONTACT_BUTTON.Size = new System.Drawing.Size(176, 34);
            this.CHANGE_CONTACT_BUTTON.TabIndex = 10;
            this.CHANGE_CONTACT_BUTTON.Text = "Изменить";
            this.CHANGE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.CHANGE_CONTACT_BUTTON.Click += new System.EventHandler(this.CHANGE_CONTACT_BUTTON_Click);
            // 
            // CREATE_CONTACT_BUTTON
            // 
            this.CREATE_CONTACT_BUTTON.Location = new System.Drawing.Point(6, 397);
            this.CREATE_CONTACT_BUTTON.Name = "CREATE_CONTACT_BUTTON";
            this.CREATE_CONTACT_BUTTON.Size = new System.Drawing.Size(176, 34);
            this.CREATE_CONTACT_BUTTON.TabIndex = 9;
            this.CREATE_CONTACT_BUTTON.Text = "Добавить контакт";
            this.CREATE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.CREATE_CONTACT_BUTTON.Click += new System.EventHandler(this.CREATE_CONTACT_BUTTON_Click);
            // 
            // tabl_kotnakt
            // 
            this.tabl_kotnakt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_kotnakt.Location = new System.Drawing.Point(0, 0);
            this.tabl_kotnakt.Name = "tabl_kotnakt";
            this.tabl_kotnakt.Size = new System.Drawing.Size(1023, 383);
            this.tabl_kotnakt.TabIndex = 0;
            // 
            // provayd
            // 
            this.provayd.Controls.Add(this.DELETE_PROV_BUTTON);
            this.provayd.Controls.Add(this.CHANGE_PROV_BUTTON);
            this.provayd.Controls.Add(this.CREATE_PROV_BUTTON);
            this.provayd.Controls.Add(this.tabl_provayder);
            this.provayd.Location = new System.Drawing.Point(4, 25);
            this.provayd.Name = "provayd";
            this.provayd.Padding = new System.Windows.Forms.Padding(3);
            this.provayd.Size = new System.Drawing.Size(1029, 437);
            this.provayd.TabIndex = 2;
            this.provayd.Text = "Провайдеры";
            this.provayd.UseVisualStyleBackColor = true;
            // 
            // DELETE_PROV_BUTTON
            // 
            this.DELETE_PROV_BUTTON.Location = new System.Drawing.Point(414, 397);
            this.DELETE_PROV_BUTTON.Name = "DELETE_PROV_BUTTON";
            this.DELETE_PROV_BUTTON.Size = new System.Drawing.Size(175, 33);
            this.DELETE_PROV_BUTTON.TabIndex = 14;
            this.DELETE_PROV_BUTTON.Text = "Удалить";
            this.DELETE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.DELETE_PROV_BUTTON.Click += new System.EventHandler(this.DELETE_PROV_BUTTON_Click);
            // 
            // CHANGE_PROV_BUTTON
            // 
            this.CHANGE_PROV_BUTTON.Location = new System.Drawing.Point(209, 397);
            this.CHANGE_PROV_BUTTON.Name = "CHANGE_PROV_BUTTON";
            this.CHANGE_PROV_BUTTON.Size = new System.Drawing.Size(176, 34);
            this.CHANGE_PROV_BUTTON.TabIndex = 13;
            this.CHANGE_PROV_BUTTON.Text = "Изменить";
            this.CHANGE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.CHANGE_PROV_BUTTON.Click += new System.EventHandler(this.CHANGE_PROV_BUTTON_Click);
            // 
            // CREATE_PROV_BUTTON
            // 
            this.CREATE_PROV_BUTTON.Location = new System.Drawing.Point(6, 397);
            this.CREATE_PROV_BUTTON.Name = "CREATE_PROV_BUTTON";
            this.CREATE_PROV_BUTTON.Size = new System.Drawing.Size(176, 34);
            this.CREATE_PROV_BUTTON.TabIndex = 12;
            this.CREATE_PROV_BUTTON.Text = "Добавить провайдер";
            this.CREATE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.CREATE_PROV_BUTTON.Click += new System.EventHandler(this.CREATE_PROV_BUTTON_Click);
            // 
            // tabl_provayder
            // 
            this.tabl_provayder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_provayder.Location = new System.Drawing.Point(0, 0);
            this.tabl_provayder.Name = "tabl_provayder";
            this.tabl_provayder.Size = new System.Drawing.Size(1026, 383);
            this.tabl_provayder.TabIndex = 0;
            // 
            // spravoch
            // 
            this.spravoch.Controls.Add(this.DELETE_PH_DICTIONARY);
            this.spravoch.Controls.Add(this.CHANGE_PH_DICTIONARY);
            this.spravoch.Controls.Add(this.CREATE_PH_DICTIONARY);
            this.spravoch.Controls.Add(this.tabl_spravochnik);
            this.spravoch.Location = new System.Drawing.Point(4, 25);
            this.spravoch.Name = "spravoch";
            this.spravoch.Padding = new System.Windows.Forms.Padding(3);
            this.spravoch.Size = new System.Drawing.Size(1029, 437);
            this.spravoch.TabIndex = 3;
            this.spravoch.Text = "Тел.справ";
            this.spravoch.UseVisualStyleBackColor = true;
            // 
            // tabl_spravochnik
            // 
            this.tabl_spravochnik.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_spravochnik.Location = new System.Drawing.Point(0, 0);
            this.tabl_spravochnik.Name = "tabl_spravochnik";
            this.tabl_spravochnik.Size = new System.Drawing.Size(1023, 383);
            this.tabl_spravochnik.TabIndex = 0;
            // 
            // FIND_PHONE_BOX
            // 
            this.FIND_PHONE_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FIND_PHONE_BOX.Location = new System.Drawing.Point(88, 19);
            this.FIND_PHONE_BOX.Name = "FIND_PHONE_BOX";
            this.FIND_PHONE_BOX.Size = new System.Drawing.Size(125, 21);
            this.FIND_PHONE_BOX.TabIndex = 2;
            this.FIND_PHONE_BOX.TextChanged += new System.EventHandler(this.FIND_PHONE_BOX_TextChanged);
            // 
            // FIND_FIO_BOX
            // 
            this.FIND_FIO_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FIND_FIO_BOX.Location = new System.Drawing.Point(337, 19);
            this.FIND_FIO_BOX.Name = "FIND_FIO_BOX";
            this.FIND_FIO_BOX.Size = new System.Drawing.Size(124, 21);
            this.FIND_FIO_BOX.TabIndex = 3;
            this.FIND_FIO_BOX.TextChanged += new System.EventHandler(this.FIND_FIO_BOX_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(22, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Телефон";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(272, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "ФИО";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.FIND_FIO_BOX);
            this.groupBox1.Controls.Add(this.FIND_PHONE_BOX);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Underline);
            this.groupBox1.Location = new System.Drawing.Point(553, 511);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(479, 52);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск по:";
            // 
            // CREATE_PH_DICTIONARY
            // 
            this.CREATE_PH_DICTIONARY.Location = new System.Drawing.Point(24, 396);
            this.CREATE_PH_DICTIONARY.Name = "CREATE_PH_DICTIONARY";
            this.CREATE_PH_DICTIONARY.Size = new System.Drawing.Size(155, 29);
            this.CREATE_PH_DICTIONARY.TabIndex = 1;
            this.CREATE_PH_DICTIONARY.Text = "Добавить";
            this.CREATE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.CREATE_PH_DICTIONARY.Click += new System.EventHandler(this.CREATE_PH_DICTIONARY_Click);
            // 
            // CHANGE_PH_DICTIONARY
            // 
            this.CHANGE_PH_DICTIONARY.Location = new System.Drawing.Point(216, 396);
            this.CHANGE_PH_DICTIONARY.Name = "CHANGE_PH_DICTIONARY";
            this.CHANGE_PH_DICTIONARY.Size = new System.Drawing.Size(154, 28);
            this.CHANGE_PH_DICTIONARY.TabIndex = 2;
            this.CHANGE_PH_DICTIONARY.Text = "Изменить";
            this.CHANGE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.CHANGE_PH_DICTIONARY.Click += new System.EventHandler(this.CHANGE_PH_DICTIONARY_Click);
            // 
            // DELETE_PH_DICTIONARY
            // 
            this.DELETE_PH_DICTIONARY.Location = new System.Drawing.Point(401, 396);
            this.DELETE_PH_DICTIONARY.Name = "DELETE_PH_DICTIONARY";
            this.DELETE_PH_DICTIONARY.Size = new System.Drawing.Size(149, 27);
            this.DELETE_PH_DICTIONARY.TabIndex = 3;
            this.DELETE_PH_DICTIONARY.Text = "Удалить";
            this.DELETE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.DELETE_PH_DICTIONARY.Click += new System.EventHandler(this.DELETE_PH_DICTIONARY_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 571);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.kontakt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.kontakt.ResumeLayout(false);
            this.abonent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).EndInit();
            this.kontak.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_kotnakt)).EndInit();
            this.provayd.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).EndInit();
            this.spravoch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_spravochnik)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl kontakt;
        private System.Windows.Forms.TabPage abonent;
        private System.Windows.Forms.TabPage kontak;
        private System.Windows.Forms.TabPage provayd;
        private System.Windows.Forms.TabPage spravoch;
        private System.Windows.Forms.DataGridView tabl_abonent;
        private System.Windows.Forms.DataGridView tabl_kotnakt;
        private System.Windows.Forms.DataGridView tabl_provayder;
        private System.Windows.Forms.DataGridView tabl_spravochnik;
        private System.Windows.Forms.TextBox FIND_PHONE_BOX;
        private System.Windows.Forms.TextBox FIND_FIO_BOX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button CREATE_button;
        private System.Windows.Forms.Button CHANGE_button;
        private System.Windows.Forms.Button DELETE_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button DELETE_CONTACT_BUTTON;
        private System.Windows.Forms.Button CHANGE_CONTACT_BUTTON;
        private System.Windows.Forms.Button CREATE_CONTACT_BUTTON;
        private System.Windows.Forms.Button DELETE_PROV_BUTTON;
        private System.Windows.Forms.Button CHANGE_PROV_BUTTON;
        private System.Windows.Forms.Button CREATE_PROV_BUTTON;
        private System.Windows.Forms.Button DELETE_PH_DICTIONARY;
        private System.Windows.Forms.Button CHANGE_PH_DICTIONARY;
        private System.Windows.Forms.Button CREATE_PH_DICTIONARY;
    }
}

