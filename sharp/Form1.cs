﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace sharp
{
    public partial class Form1 : Form
    {
        private string ConnectionString = global::sharp.Properties.Settings.Default.Database1ConnectionString;
        private const string AbonentTableName = "Vahomskaya_abonent";
        private const string ContactTableName = "Vahomskaya_contact";
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGW();
            updateContactDGW();
            updatePROVDGW();
            updatePHONEDGW();
        }

        /*private void find_Click(object sender, EventArgs e)
        {

            /*SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ;
            conn.Open();
            SqlCommand com = conn.CreateCommand();
            com.CommandText = "SELECT * From Vahomskaya_abonent";
            SqlDataReader reader = com.ExecuteReader();
            string names = " ";
            while (reader.Read())
            {
                names += reader["name"].ToString() + " ";
            }
            conn.Close();
            MessageBox.Show("name");
          
            string query = "SELECT surname FROM Vahomskaya_abonent";
            SqlDataAdapter aOrder = new SqlDataAdapter(query, ConnectionString);
            DataTable table = new DataTable();
            aOrder.Fill(table);
            string res = " ";

            foreach (DataRow row in table.Rows)
            {
                res += row["surname"].ToString();
            }
            MessageBox.Show(res);
        }*/

        void updateAbonentDGW()
        {
            var request = "SELECT * FROM [" + AbonentTableName + "] WHERE surname + ' ' + name + ' ' +patronymic LIKE '%" + FIND_FIO_BOX.Text + "%'";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);

            tabl_abonent.DataSource = abonentTable;

            tabl_abonent.Columns["id"].Visible = false;
            tabl_abonent.Columns["name"].HeaderText = "Имя";
            tabl_abonent.Columns["surname"].HeaderText = "Фамилия";
            tabl_abonent.Columns["patronymic"].HeaderText = "Отчество";
            tabl_abonent.Columns["birthdate"].HeaderText = "Дата Рождения";
            tabl_abonent.Columns["adress"].HeaderText = "Адрес";
            tabl_abonent.Columns["comment"].HeaderText = "Комментраий";

        }
        void updateContactDGW()
        {
            var request = "SELECT * FROM [" + ContactTableName + "] WHERE phone LIKE '%" + FIND_PHONE_BOX.Text + "%'";
           // var request = "SELECT * FROM Vahomskaya_contact";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var kontakTable = new DataTable();
            adapter.Fill(kontakTable);
            tabl_kotnakt.DataSource = kontakTable;
            tabl_kotnakt.Columns["id"].Visible = false;
            tabl_kotnakt.Columns["provider_id"].Visible = false;
            tabl_kotnakt.Columns["phone"].HeaderText = "Телефон";
            tabl_kotnakt.Columns["type"].HeaderText = "Тип";
        }

        void updatePROVDGW()
        {
            var request = "SELECT * FROM Vahomskaya_provider";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var provTable = new DataTable();
            adapter.Fill(provTable);
            tabl_provayder.DataSource = provTable;
            tabl_provayder.Columns["id"].Visible = false;
            tabl_provayder.Columns["name"].HeaderText = "Имя";
            tabl_provayder.Columns["score"].HeaderText = "Рейтинг";
        }

        void updatePHONEDGW()
        {
            var request = @"SELECT *FROM Vahomskaya_abonent JOIN  Vahomskaya_abonent_has_contact ON Vahomskaya_abonent.id= Vahomskaya_abonent_has_contact.abonent_id 
            JOIN Vahomskaya_contact ON Vahomskaya_contact.id= Vahomskaya_abonent_has_contact.contact_id LEFT JOIN Vahomskaya_provider ON Vahomskaya_provider.id=Vahomskaya_contact.provider_id ";

            if (FIND_PHONE_BOX.Text != "")
            {
                request += @"WHERE  Vahomskaya_contact.phone LIKE+'%" + FIND_PHONE_BOX.Text + "%'";

            }

            if (FIND_FIO_BOX.Text != "")
            {
                request += @" WHERE Vahomskaya_abonent.surname+' '   +Vahomskaya_abonent.patronymic+' ' +Vahomskaya_abonent.name  LIKE+'%" + FIND_FIO_BOX.Text + "%'";
            }

            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonTable = new DataTable();
            adapter.Fill(abonTable);
            tabl_spravochnik.DataSource = abonTable;


            tabl_spravochnik.Columns["Id"].Visible = false;
            tabl_spravochnik.Columns["adress"].Visible = false;
            tabl_spravochnik.Columns["birthdate"].Visible = false;
            tabl_spravochnik.Columns["comment"].Visible = false;
            tabl_spravochnik.Columns["contact_id"].Visible = false;
            tabl_spravochnik.Columns["abonent_id"].Visible = false;
          
            tabl_spravochnik.Columns["type"].Visible = false;
            tabl_spravochnik.Columns["provider_id"].Visible = false;
            tabl_spravochnik.Columns["score"].Visible = false;
            tabl_spravochnik.Columns["name"].HeaderText = "Имя";
            tabl_spravochnik.Columns["surname"].HeaderText = "Фамилия";
            tabl_spravochnik.Columns["patronymic"].HeaderText = "Отчество";
            tabl_spravochnik.Columns["phone"].HeaderText = "Номер телефона";
            tabl_spravochnik.Columns["Id1"].Visible = false;
            tabl_spravochnik.Columns["Id2"].Visible = false;
            tabl_spravochnik.Columns["name1"].HeaderText = "Имя провайдера";
        }

        private void FIND_PHONE_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateContactDGW();
        }

        private void FIND_FIO_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateAbonentDGW();
        }

        //АБОНЕНТ
        private void CREATE_button_Click(object sender, EventArgs e)
        {

            var form = new AbForm();
            var res = form.ShowDialog();
            if(res==DialogResult.OK)
            {
               
                var surname = form.AbForm_SURNAME_TextBox.Text;
                var name = form.AbForm_NAME_TextBox.Text;
                var patronymic = form.AbForm_PATRONYMIC_TextBox.Text;
                var birthdate = form.AbForm_BIRTHDATE_TextBox.Text;
                var adress = form.AbForm_ADRESS_TextBox.Text;
                var comment = form.AbForm_COMMENT_TextBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Vahomskaya_abonent (name, surname, patronymic, birthdate, adress, comment)" + "VALUES ('" + name + "', '" + surname + "', '"
                    + patronymic + "', '" + birthdate + "', '" + adress + "', '" + comment + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGW();
            }
        }

        private void CHANGE_button_Click(object sender, EventArgs e)
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbForm();
            form.AbForm_SURNAME_TextBox.Text = row.Cells["surname"].Value.ToString();
            form.AbForm_NAME_TextBox.Text = row.Cells["name"].Value.ToString();
            form.AbForm_PATRONYMIC_TextBox.Text = row.Cells["patronymic"].Value.ToString();
            form.AbForm_ADRESS_TextBox.Text = row.Cells["adress"].Value.ToString();
            form.AbForm_COMMENT_TextBox.Text= row.Cells["comment"].Value.ToString();
            form.AbForm_BIRTHDATE_TextBox.Text = row.Cells["birthdate"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.AbForm_SURNAME_TextBox.Text;
                var name = form.AbForm_NAME_TextBox.Text;
                var patronymic = form.AbForm_PATRONYMIC_TextBox.Text;
                var comment = form.AbForm_COMMENT_TextBox.Text;
                var address = form.AbForm_ADRESS_TextBox.Text;
                var birthday = form.AbForm_BIRTHDATE_TextBox.Text;
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Vahomskaya_abonent SET surname = '" + surname + "', name = '" + name + "', " +
                    "patronymic = '" + patronymic + "', adress = '" + address + "', comment = '" + comment + "', birthdate = '" + birthday + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateAbonentDGW();
            }
        }

        private void DELETE_button_Click(object sender, EventArgs e)
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Vahomskaya_abonent WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateAbonentDGW();
        }

        //КОНТАКТ
        private void CREATE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var form = new ConForm();
            {
                var getReq = "SELECT *FROM Vahomskaya_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow row in providerTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }
            
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number_TextBox.Text;
                var type = form.ConForm_Type_TextBox.Text;
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Vahomskaya_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '" + provider_id.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactDGW();
            }
        }

        private void CHANGE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_kotnakt.SelectedRows.Count > 0 ? tabl_kotnakt.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new ConForm();
            form.ConForm_Phone_Number_TextBox.Text = row.Cells["phone"].Value.ToString();
            form.ConForm_Type_TextBox.Text = row.Cells["type"].Value.ToString();
            {
                var getReq = "SELECT *FROM Vahomskaya_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderID = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number_TextBox.Text;
                var type = form.ConForm_Type_TextBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Vahomskaya_contact SET phone = '" + phone + "', type = '" + type + "', provider_id=" + provider_id.ToString()+
                     "WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactDGW();
            }
        }

        private void DELETE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_kotnakt.SelectedRows.Count > 0 ? tabl_kotnakt.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Vahomskaya_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateContactDGW();
        }

        //ПРОВАЙДЕР
        private void CREATE_PROV_BUTTON_Click(object sender, EventArgs e)
        {
            var form = new ProvForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.ProvForm_Name_TextBox.Text;
                var score = form.ProvForm_Score_TextBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Vahomskaya_provider (name, score) VALUES ('" + name + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updatePROVDGW();
            }

        }

        private void CHANGE_PROV_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new ProvForm();
            form.ProvForm_Name_TextBox.Text = row.Cells["name"].Value.ToString();
            form.ProvForm_Score_TextBox.Text = row.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.ProvForm_Name_TextBox.Text;
                var score = form.ProvForm_Score_TextBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Vahomskaya_provider SET name = '" + name + "', score = '" + score + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updatePROVDGW();
            }
        }

        private void DELETE_PROV_BUTTON_Click(object sender, EventArgs e)
        {

            
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Vahomskaya_provider WHERE Id = " + id+ ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updatePROVDGW();
            updateContactDGW();
            updatePHONEDGW();
            updateAbonentDGW();
        }

        //ТЕЛЕФОННЫЙ СПРАВОЧНИК
        private void CREATE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {
            var form = new PhDirecForm();

            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.ContactData = dict;
            }

            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "INSERT INTO Vahomskaya_abonent_has_contact" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();

                updatePHONEDGW();
            }
        }

        private void CHANGE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {

            var row2= tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row2 == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new PhDirecForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }


                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }


                form.ContactData = dict;
            }
            DataGridViewSelectedRowCollection Row = tabl_spravochnik.SelectedRows;

            var mas = tabl_spravochnik.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Vahomskaya_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Vahomskaya_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Vahomskaya_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.FIO_PhDirecForm_ComBox.Text = str;

            req = "SELECT phone FROM Vahomskaya_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.Phone_PhDirecForm_ComBox.Text = last.ToString();

            conn.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Vahomskaya_abonent_has_contact "  + " SET contact_id='" + form.ContactID +
                    "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();


                updatePHONEDGW();
            }
        }

        private void DELETE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {
           
            var row = tabl_spravochnik.SelectedRows.Count > 0 ? tabl_spravochnik.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Сначала выберите строчку!!!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Vahomskaya_abonent_has_contact WHERE contact_id = '" + num2 + 
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updatePHONEDGW();
        }
    } 
}
