﻿namespace sharp
{
    partial class PhDirecForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FIO_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.Phone_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PhDirecCancel_BUTTON = new System.Windows.Forms.Button();
            this.PhDirecOK_BUTTON = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FIO_PhDirecForm_ComBox
            // 
            this.FIO_PhDirecForm_ComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FIO_PhDirecForm_ComBox.Location = new System.Drawing.Point(147, 30);
            this.FIO_PhDirecForm_ComBox.Name = "FIO_PhDirecForm_ComBox";
            this.FIO_PhDirecForm_ComBox.Size = new System.Drawing.Size(175, 21);
            this.FIO_PhDirecForm_ComBox.TabIndex = 0;
            // 
            // Phone_PhDirecForm_ComBox
            // 
            this.Phone_PhDirecForm_ComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Phone_PhDirecForm_ComBox.FormattingEnabled = true;
            this.Phone_PhDirecForm_ComBox.Location = new System.Drawing.Point(147, 69);
            this.Phone_PhDirecForm_ComBox.Name = "Phone_PhDirecForm_ComBox";
            this.Phone_PhDirecForm_ComBox.Size = new System.Drawing.Size(173, 21);
            this.Phone_PhDirecForm_ComBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ФИО";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Номер телефона";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.PhDirecCancel_BUTTON);
            this.groupBox1.Controls.Add(this.PhDirecOK_BUTTON);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Phone_PhDirecForm_ComBox);
            this.groupBox1.Controls.Add(this.FIO_PhDirecForm_ComBox);
            this.groupBox1.Location = new System.Drawing.Point(16, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 147);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тел.справочник";
            // 
            // PhDirecCancel_BUTTON
            // 
            this.PhDirecCancel_BUTTON.Location = new System.Drawing.Point(241, 108);
            this.PhDirecCancel_BUTTON.Name = "PhDirecCancel_BUTTON";
            this.PhDirecCancel_BUTTON.Size = new System.Drawing.Size(79, 27);
            this.PhDirecCancel_BUTTON.TabIndex = 7;
            this.PhDirecCancel_BUTTON.Text = "Cancel";
            this.PhDirecCancel_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecCancel_BUTTON.Click += new System.EventHandler(this.PhDirecCancel_BUTTON_Click);
            // 
            // PhDirecOK_BUTTON
            // 
            this.PhDirecOK_BUTTON.Location = new System.Drawing.Point(147, 108);
            this.PhDirecOK_BUTTON.Name = "PhDirecOK_BUTTON";
            this.PhDirecOK_BUTTON.Size = new System.Drawing.Size(78, 27);
            this.PhDirecOK_BUTTON.TabIndex = 6;
            this.PhDirecOK_BUTTON.Text = "OK";
            this.PhDirecOK_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecOK_BUTTON.Click += new System.EventHandler(this.PhDirecOK_BUTTON_Click);
            // 
            // PhDirecForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 171);
            this.Controls.Add(this.groupBox1);
            this.Name = "PhDirecForm";
            this.Text = "PhDirecForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox FIO_PhDirecForm_ComBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button PhDirecCancel_BUTTON;
        private System.Windows.Forms.Button PhDirecOK_BUTTON;
        public System.Windows.Forms.ComboBox Phone_PhDirecForm_ComBox;
    }
}