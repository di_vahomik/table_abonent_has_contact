﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sharp
{
    public partial class PhDirecForm : Form
    {
        public PhDirecForm()
        {
            InitializeComponent();
        }

        private void PhDirecOK_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void PhDirecCancel_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        public Dictionary<int, string> AbonentData
        {
            set
            {
                FIO_PhDirecForm_ComBox.DataSource = value.ToArray();
                FIO_PhDirecForm_ComBox.DisplayMember = "Value";
            }
        }

        public int AbonentID
        {
            get { return ((KeyValuePair<int, string>)FIO_PhDirecForm_ComBox.SelectedItem).Key; }
        }

        public Dictionary<int, string> ContactData
        {
            set
            {
                Phone_PhDirecForm_ComBox.DataSource = value.ToArray();
                Phone_PhDirecForm_ComBox.DisplayMember = "Value";
            }
        }

        public int ContactID
        {
            get { return ((KeyValuePair<int, string>)Phone_PhDirecForm_ComBox.SelectedItem).Key; }
        }
    }
}
