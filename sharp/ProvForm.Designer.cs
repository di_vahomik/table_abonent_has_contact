﻿namespace sharp
{
    partial class ProvForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AbFom_CANCEL_Button = new System.Windows.Forms.Button();
            this.AbFom_OK_Button = new System.Windows.Forms.Button();
            this.ProvForm_Score_TextBox = new System.Windows.Forms.TextBox();
            this.ProvForm_Name_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.ProvForm_Score_TextBox);
            this.groupBox1.Controls.Add(this.AbFom_CANCEL_Button);
            this.groupBox1.Controls.Add(this.ProvForm_Name_TextBox);
            this.groupBox1.Controls.Add(this.AbFom_OK_Button);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 131);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Провайдер";
            // 
            // AbFom_CANCEL_Button
            // 
            this.AbFom_CANCEL_Button.Location = new System.Drawing.Point(229, 90);
            this.AbFom_CANCEL_Button.Name = "AbFom_CANCEL_Button";
            this.AbFom_CANCEL_Button.Size = new System.Drawing.Size(74, 24);
            this.AbFom_CANCEL_Button.TabIndex = 15;
            this.AbFom_CANCEL_Button.Text = "Cancel";
            this.AbFom_CANCEL_Button.UseVisualStyleBackColor = true;
            this.AbFom_CANCEL_Button.Click += new System.EventHandler(this.AbFom_CANCEL_Button_Click);
            // 
            // AbFom_OK_Button
            // 
            this.AbFom_OK_Button.Location = new System.Drawing.Point(151, 90);
            this.AbFom_OK_Button.Name = "AbFom_OK_Button";
            this.AbFom_OK_Button.Size = new System.Drawing.Size(74, 24);
            this.AbFom_OK_Button.TabIndex = 14;
            this.AbFom_OK_Button.Text = "OK";
            this.AbFom_OK_Button.UseVisualStyleBackColor = true;
            this.AbFom_OK_Button.Click += new System.EventHandler(this.AbFom_OK_Button_Click);
            // 
            // ProvForm_Score_TextBox
            // 
            this.ProvForm_Score_TextBox.Location = new System.Drawing.Point(151, 52);
            this.ProvForm_Score_TextBox.Name = "ProvForm_Score_TextBox";
            this.ProvForm_Score_TextBox.Size = new System.Drawing.Size(145, 20);
            this.ProvForm_Score_TextBox.TabIndex = 20;
            // 
            // ProvForm_Name_TextBox
            // 
            this.ProvForm_Name_TextBox.Location = new System.Drawing.Point(151, 24);
            this.ProvForm_Name_TextBox.Name = "ProvForm_Name_TextBox";
            this.ProvForm_Name_TextBox.Size = new System.Drawing.Size(145, 20);
            this.ProvForm_Name_TextBox.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Рейтинг провайдера";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Название провайдера";
            // 
            // ProvForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 153);
            this.Controls.Add(this.groupBox1);
            this.Name = "ProvForm";
            this.Text = "ProvForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AbFom_CANCEL_Button;
        private System.Windows.Forms.Button AbFom_OK_Button;
        public System.Windows.Forms.TextBox ProvForm_Score_TextBox;
        public System.Windows.Forms.TextBox ProvForm_Name_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}